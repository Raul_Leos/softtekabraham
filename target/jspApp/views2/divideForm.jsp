<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Divide</title>
<%@ include file="bootstrap.html" %>
</head>
<body>
	<form action="exceptionHandler.jsp" class="form-group container mt-5 mb-5 ml-5 mr-5">
		<label for="num1">Numero 1: </label>
		<input type="number" class="form-control" name="num1" id="" value="1" />
		<label for="num2">Numero 2: </label>
		<input type="number" class="form-control" name="num2" id="" value="1" />
		<button type="submit" class="btn btn-primary mt-5">Calcular</button>
	</form>
</body>
</html>