<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Set Session Example</title>
</head>
<body>
	<%
		String name = request.getParameter("username");
		out.println("Welcome: " + name);
		session.setAttribute("user", name);
	%>
	<a href="readSession.jsp">Read Session</a>
</body>
</html>