<%
	String title="Index Page";
	String message = "Ejemplos de JSP";
%>
<%!
	public String getDate(){
		return java.util.Calendar.getInstance().getTime().toString();
	}
%>

<html>
<head>
	<title><%= title %></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<%@ include file="./views/bootstrap.html" %>
</head>
<body>
	<div class="col-sm-12 container mr-5 ml-5 pl-5 pr-5 mt-5 ">
		<h1><%= message %></h1>
		<ul>
			<li><a href="./views/welcome.jsp">Welcome</a></li>
			<li><a href="./views/form.jsp">Form</a></li>
			<li><a href="configObjectExample">Config Object Example</a></li>
			<li><a href="./views/sessionObject.jsp">Session Example</a></li>
			<li><a href="./views/includeDirectiveExample.jsp">Include Example</a></li>
			<li><a href="./views/pageDirectiveExample.jsp">Page directive import example</a></li>
			<li><a href="./views/divideForm.jsp">Exception Handler</a></li>
			<li><a href="./views/formUser.jsp">Model view Controller Example</a></li>
		</ul>
		<br />
		<%= getDate() %>
	</div>
</body>
</html>