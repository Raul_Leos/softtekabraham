<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Create To Do Task</title>
<%@ include file="./bootstrap.html" %>
</head>
<body>
	<div class="container">
		<h1>Create Task</h1>
		<form action="http://localhost:8080/jspApp/ToDoServlet" class="form-group" method="POST" >
			<label for="description">Descripcion:</label>
			<input type="text" name="description" id="description" class="form-control" required />
			<button class="btn btn-primary mt-2" type="submit">Add Task</button>
		</form>
	</div>
</body>
</html>