<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ page import = "com.softtek.academy.javaweb.model.Task" %>
<%@ page import = "com.softtek.academy.javaweb.dao.ToDoDao" %>
<%@ page import = "java.util.List" %>

    
<% 
	List<Task> tasks = ToDoDao.getDone();
	request.setAttribute("tasks", tasks);
%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>To Do List</title>
<%@ include file='./bootstrap.html' %>
</head>
<body>
<div class="col-sm-12 container mr-5 ml-5 pl-5 pr-5 mt-5 ">
		<h1>Done List</h1>
		
		<a href="http://localhost:8080/jspApp/views/toDoList.jsp" class="btn btn-info mt-5 mb-5">To Do List</a>
		<div class="col-sm-12">
			<table class="table">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Id</th>
			      <th scope="col">Description</th>
			      <th scope="col">Status</th>
			    </tr>
			  </thead>
			  <tbody>
			  <c:forEach items="${tasks}" var="t">
			  <tr>
				  <th scope="row"><c:out value = "${t.getId()}"/></th>
			      <td><c:out value = "${t.getDescription()}"/></td>
			      <td><h3 class="text-danger">Done</h3></td>
		    	</tr>
			</c:forEach>
			  </tbody>
			</table>
		</div>
	</div>
</body>
</html>