<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ page import = "com.softtek.academy.javaweb.model.Task" %>
<%@ page import = "com.softtek.academy.javaweb.dao.ToDoDao" %>
<%@ page import = "java.util.List" %>

    
<% 
	List<Task> tasks = ToDoDao.getUndone();
	request.setAttribute("tasks", tasks);
%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>To Do List</title>
<%@ include file='./bootstrap.html' %>
<script>
  window.onload = ()=>{
	  	if(window.location.pathname.toString() == "/jspApp/ToDoServlet"){
		  	window.location.pathname = "jspApp/views/toDoList.jsp";
			console.log("true");
		}else{
			console.log("g")
			}
	  }
</script>
</head>
<body>
<div class="col-sm-12 container mr-5 ml-5 pl-5 pr-5 mt-5 ">
		<h1>To Do List</h1>
		
		<div class="row">
				<div class="col-sm-6"><a href="createTask.jsp" class="btn btn-info mt-5 mb-5">Create Task</a></div>
				<div class="col-sm-6"><a href="doneList.jsp" class="btn btn-warning mt-5 mb-5">Done Task list</a></div>
		</div>
		
		<div class="col-sm-12">
			<table class="table">
			  <thead class="thead-dark">
			    <tr>
			      <th scope="col">Id</th>
			      <th scope="col">Description</th>
			      <th scope="col">Done task</th>
			    </tr>
			  </thead>
			  <tbody>
			  <c:forEach items="${tasks}" var="t">
			  <tr>
				  <th scope="row"><c:out value = "${t.getId()}"/></th>
			      <td><c:out value = "${t.getDescription()}"/></td>
			      <form action="http://localhost:8080/jspApp/ToDoServlet" align="center">
			      		<input type="text" name="id" id="id" value="${t.getId()}" hidden />
			      	<td><button type="submit"  class="btn btn-success">X</button></td>
			      </form>
		    	</tr>
			</c:forEach>
			  </tbody>
			</table>
		</div>
	</div>
</body>
</html>