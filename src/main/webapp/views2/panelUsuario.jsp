<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "com.softtek.academy.javaweb.model.Color" %>
<%@ page import = "java.util.List" %>
<%@ page import = "com.softtek.academy.javaweb.dao.ColorDao" %>
<%@ page import = "com.softtek.academy.javaweb.model.User" %>

<%
	User usuario = (User)request.getAttribute("user");
	List<Color> colores = ColorDao.getAll();
	request.setAttribute("colores", colores);
	//Obtener la lista de colores
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<meta charset="ISO-8859-1">
<%@ include file="./bootstrap.html" %>
<title>Correct Login</title>
</head>
<body>
	<h1>
		Datos Correctos! <br />
		Bienvenido: <%= usuario.getUsername() %> <br />
		Contraseņa: <%= usuario.getPassword() %>
	</h1>
	
	<h2>Colores</h2>
		<ul>
			<% try{for(Color color : colores){
				%><li><%=color.getName()%></li><%
			}} catch(Exception e){%><%=e%><%} %>
		</ul>
	<c:forEach items="${colores }" var="color">
		<c:out value = "${color.getName()}"/>
	</c:forEach>
</body>
</html>