<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Form User</title>
<%@ include file="bootstrap.html" %>

</head>
<body>
	
	
	<form action="http://localhost:8080/jspApp/ControllerServlet" class="form-group container mt-5 mb-5 ml-5 mr-5" method="POST">
		<label for="username">Username: </label>
		<input type="text" class="form-control" name="username" id="" placeholder="SamGod" />
		<label for="password">Password: </label>
		<input type="password" class="form-control" name="password" id="" placeholder="*****" />
		<button type="submit" class="btn btn-primary mt-5">Iniciar Sesion</button>
	</form>
</body>
</html>