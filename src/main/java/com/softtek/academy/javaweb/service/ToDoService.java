package com.softtek.academy.javaweb.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.model.Task;

public class ToDoService {
	public static boolean createTask(String description) {
		try {
			Connection con = DbConnection.getConection();
			PreparedStatement ps = con.prepareStatement(
					"INSERT INTO to_do_list (list,is_done) VALUES (?,?);"
					);
			ps.setString(1, description);
			ps.setInt(2, 0);
			ps.execute();
			
		}catch(Exception e) {
			System.out.println(e);
			return false;
			
		}	
		return true;
	}
	
	public static boolean updateStatus(int id) {
		 
		try {
			Connection con = DbConnection.getConection();
			PreparedStatement ps = con.prepareStatement(
					"UPDATE to_do_list SET is_done = 1 WHERE id=?;"
					);
			ps.setInt(1, id);
			ps.execute();
			
		}catch(Exception e) {
			System.out.println(e);
			return false;
		}	
		
		return true;
	}
	
	public static ResultSet getDone() {
		 
		try {
			Connection con = DbConnection.getConection();
			PreparedStatement ps = con.prepareStatement(
						"SELECT * FROM to_do_list WHERE is_done=1"
					);
			ResultSet rs = ps.executeQuery();
			return rs;
		}catch(Exception e) {
			System.out.println(e);
			return null;
		}
	}
	
	public static ResultSet getUndone() {
		 
		try {
			Connection con = DbConnection.getConection();
			PreparedStatement ps = con.prepareStatement(
						"SELECT * FROM to_do_list WHERE is_done = 0"
					);
			ResultSet rs = ps.executeQuery();
			return rs;
		}catch(Exception e) {
			System.out.println(e);
			return null;
		}
	}
}
