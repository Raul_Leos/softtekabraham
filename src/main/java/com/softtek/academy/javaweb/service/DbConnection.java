package com.softtek.academy.javaweb.service;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbConnection {
	public static Connection getConection() {
		final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		final String DB_URL = "jdbc:mysql://localhost:3306/jsp"
				+ "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		final String USER = "root";
		final String PASS = "1234";
		
		Connection con = null;
		
		try {
			Class.forName(JDBC_DRIVER);
			con = DriverManager.getConnection(DB_URL,USER,PASS);
		}catch(Exception e) {
			System.out.println(e);
		}
		
		return con;
	}
}
