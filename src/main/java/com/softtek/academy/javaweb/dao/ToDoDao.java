package com.softtek.academy.javaweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.model.Color;
import com.softtek.academy.javaweb.model.Task;
import com.softtek.academy.javaweb.service.DbConnection;
import com.softtek.academy.javaweb.service.ToDoService;

public class ToDoDao {
	
	public static boolean createTask(String description) {
		return 	ToDoService.createTask(description);
	}
	
	public static boolean updateStatus(int id) {
		 
		return ToDoService.updateStatus(id);	
	}
	
	
	public static List<Task> getDone() {
		List<Task> tasks = new ArrayList<Task>();
		 
		try {
			ResultSet rs = ToDoService.getDone();
			while(rs.next()) {
				Task task = new Task();
				task.setId(rs.getInt("id"));
				task.setDescription(rs.getString("list"));
				task.setStatus(rs.getBoolean("is_done"));
				tasks.add(task);
			}
		}catch(Exception e) {
			System.out.println(e);
			return tasks;
		}
		return tasks;
	}
	
	public static List<Task> getUndone() {
		List<Task> tasks = new ArrayList<Task>();
		 
		try {
			ResultSet rs = ToDoService.getUndone();
			while(rs.next()) {
				Task task = new Task();
				task.setId(rs.getInt("id"));
				task.setDescription(rs.getString("list"));
				task.setStatus(rs.getBoolean("is_done"));
				tasks.add(task);
			}
		}catch(Exception e) {
			System.out.println(e);
			return tasks;
		}
		return tasks;
	}
}
