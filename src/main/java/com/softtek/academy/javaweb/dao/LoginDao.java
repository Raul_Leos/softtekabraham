package com.softtek.academy.javaweb.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.softtek.academy.javaweb.model.User;

public class LoginDao {
	public static Connection getConection() {
		final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		final String DB_URL = "jdbc:mysql://localhost:3306/jsp"
				+ "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		final String USER = "root";
		final String PASS = "1234";
		
		Connection con = null;
		
		try {
			Class.forName(JDBC_DRIVER);
			con = DriverManager.getConnection(DB_URL,USER,PASS);
		}catch(Exception e) {
			System.out.println(e);
		}
		
		return con;
	}
	
	public static User getUser(String name) {
		User user = null;
		 
		try {
			Connection con = getConection();
			PreparedStatement ps = con.prepareStatement(
						"SELECT * FROM USER WHERE NAME=?"
					);
			ps.setString(1, name);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				user = new User();
				user.setUsername(rs.getString("name"));
				user.setPassword(rs.getString("password"));
			}
		}catch(Exception e) {
			System.out.println(e);
			return user;
		}
		return user;
	}
}
