package com.softtek.academy.javaweb.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academy.javaweb.model.Color;
import com.softtek.academy.javaweb.model.User;

public class ColorDao {
	public static Connection getConection() {
		final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
		final String DB_URL = "jdbc:mysql://localhost:3306/jsp"
				+ "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
		final String USER = "root";
		final String PASS = "1234";
		
		Connection con = null;
		
		try {
			Class.forName(JDBC_DRIVER);
			con = DriverManager.getConnection(DB_URL,USER,PASS);
		}catch(Exception e) {
			System.out.println(e);
		}
		
		return con;
	}
	
	public static List<Color> getAll() {
		List<Color> colores = new ArrayList<Color>();
		 
		try {
			Connection con = getConection();
			PreparedStatement ps = con.prepareStatement(
						"SELECT * FROM COLOR"
					);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Color color = new Color();
				color.setName(rs.getString("name"));
				color.setHexValue(rs.getString("hexValue"));
				colores.add(color);
				
			}
		}catch(Exception e) {
			System.out.println(e);
			return colores;
		}
		return colores;
	}
}
