package com.softtek.academy.javaweb.model;

public class Color {
	private String name;
	private String hexValue;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHexValue() {
		return hexValue;
	}
	public void setHexValue(String hexValue) {
		this.hexValue = hexValue;
	}
	@Override
	public String toString() {
		return "Color [name=" + name + ", hexValue=" + hexValue + "]";
	}
	
	
}
