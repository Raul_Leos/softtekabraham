package com.softtek.academy.javaweb.model;

import com.softtek.academy.javaweb.dao.LoginDao;

public class User {
	String username;
	String password;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean credencialesCorrectas() {
		
		User result = LoginDao.getUser(this.username);
		try {
			if(result.getUsername().toString().equals(this.username) && 
					result.getPassword().toString().equals(this.password)) {
				return true;
			}else {
				return false;
			}
		}catch(Exception e) {
			System.out.println(e);
			return false;
		}
	}
	
	
	
}
