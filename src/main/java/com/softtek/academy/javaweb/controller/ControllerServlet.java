package com.softtek.academy.javaweb.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.dao.ColorDao;
import com.softtek.academy.javaweb.model.Color;
import com.softtek.academy.javaweb.model.User;

/**
 * Servlet implementation class ControllerServlet
 */
public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		//Crear el modelo
		User usuario = new User();
		usuario.setPassword(password);
		usuario.setUsername(username);
		
		
		if(usuario.credencialesCorrectas()) {
			RequestDispatcher view = request.getRequestDispatcher("./views/panelUsuario.jsp");
			request.setAttribute("user",usuario);
			view.forward(request, response);
		}else {
			RequestDispatcher view = request.getRequestDispatcher("./views/errorPage.jsp");
			view.forward(request, response);
		}
	}

}
